Rails.application.routes.draw do

  get 'teacher/index'

  get 'teacher/modules'

  get 'workers/index'

  get 'modules/index'

  get 'groups/index'

  get 'system/index'

  root 'index#index'

  get 'index/index'

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  get 'dashboard' => 'dashboard#index'
  get 'dashboard/admin' => 'dashboard#admin'

  get 'settings' => 'system#index'

  #user
  post '/change/lang' => 'users#change_locale'

  #workers
  get 'management/workers' => 'workers#index'
  get 'management/worker/new' => 'workers#new'

  #students
  get 'management/students' => 'students#index'
  get 'management/student/new' => 'students#new'

  #modules
  get 'management/modules' => 'modules#index'
  get 'management/module/new' => 'modules#new'
  post 'management/module/create' => 'modules#create'

  #groups
  get 'management/groups' => 'groups#index'
  get 'management/group/new' => 'groups#new'
  post 'management/group/new' => 'groups#create'

  get 'storage/group/:id' => 'groups#show'
  get 'storage/module/:id' => 'modules#show'

  #API JSON
  get 'api' => 'api#index'
  get 'api/departments' => 'api#departments'

  get 'api/professions' => 'api#professions'
  get 'api/professions/:department' => 'api#professions'

  get 'api/groups' => 'api#groups'
  get 'api/groups/:profession' => 'api#groups'

  get 'api/students' => 'api#students'

  #teacher
  get 'teacher/modules' => 'teacher#modules'


  get 'teacher/module/show/:id' => 'teacher#show_module'
  get 'teacher/module/edit/:id' => 'teacher#edit_module'
  post 'teacher/module/edit/:id' => 'teacher#update_module'



  resources :users
  resources :students
  resources :groups
  resources :dashboard, only: :index

  scope :management do
    resources :workers
    resources :professions
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
