# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160310183925) do

  create_table "chairs", force: :cascade do |t|
    t.string  "name"
    t.integer "department_id"
  end

  add_index "chairs", ["department_id"], name: "index_chairs_on_department_id"

  create_table "departments", force: :cascade do |t|
    t.string  "name"
    t.integer "dean"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "year"
    t.integer "profession_id"
    t.string  "alias"
    t.boolean "closed"
  end

  add_index "groups", ["profession_id"], name: "index_groups_on_profession_id"

  create_table "mixed_groups", force: :cascade do |t|
    t.integer "department_id"
    t.string  "alias"
    t.string  "description"
    t.date    "year"
    t.boolean "closed"
  end

  add_index "mixed_groups", ["department_id"], name: "index_mixed_groups_on_department_id"

  create_table "mixed_groups_students", id: false, force: :cascade do |t|
    t.integer "student_id"
    t.integer "mixed_group_id"
  end

  add_index "mixed_groups_students", ["mixed_group_id"], name: "index_mixed_groups_students_on_mixed_group_id"
  add_index "mixed_groups_students", ["student_id", "mixed_group_id"], name: "index_mixed_groups_students_on_student_id_and_mixed_group_id", unique: true
  add_index "mixed_groups_students", ["student_id"], name: "index_mixed_groups_students_on_student_id"

  create_table "module_deadlines", force: :cascade do |t|
    t.integer  "department_id"
    t.integer  "profession_id"
    t.integer  "group_id"
    t.integer  "mixed_group_id"
    t.integer  "subject_id"
    t.integer  "lecturer_id"
    t.integer  "assistant_id"
    t.integer  "dean_id"
    t.boolean  "is_signed",                 default: false
    t.integer  "study_year_id"
    t.integer  "semester"
    t.integer  "count_of_modules"
    t.integer  "module_max_score_1"
    t.integer  "module_max_score_2"
    t.integer  "module_max_score_3"
    t.integer  "module_max_score_4"
    t.integer  "module_max_score_5"
    t.integer  "individual_task_max_score"
    t.integer  "exam_max_score"
    t.date     "deadline_module_1"
    t.date     "deadline_module_2"
    t.date     "deadline_module_3"
    t.date     "deadline_module_4"
    t.date     "deadline_module_5"
    t.date     "deadline_individual_task"
    t.date     "deadline_checkout"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "module_deadlines", ["deadline_individual_task"], name: "index_module_deadlines_on_deadline_individual_task"
  add_index "module_deadlines", ["deadline_module_1"], name: "index_module_deadlines_on_deadline_module_1"
  add_index "module_deadlines", ["deadline_module_2"], name: "index_module_deadlines_on_deadline_module_2"
  add_index "module_deadlines", ["deadline_module_3"], name: "index_module_deadlines_on_deadline_module_3"
  add_index "module_deadlines", ["deadline_module_4"], name: "index_module_deadlines_on_deadline_module_4"
  add_index "module_deadlines", ["deadline_module_5"], name: "index_module_deadlines_on_deadline_module_5"
  add_index "module_deadlines", ["department_id"], name: "index_module_deadlines_on_department_id"
  add_index "module_deadlines", ["exam_max_score"], name: "index_module_deadlines_on_exam_max_score"
  add_index "module_deadlines", ["group_id"], name: "index_module_deadlines_on_group_id"
  add_index "module_deadlines", ["individual_task_max_score"], name: "index_module_deadlines_on_individual_task_max_score"
  add_index "module_deadlines", ["is_signed"], name: "index_module_deadlines_on_is_signed"
  add_index "module_deadlines", ["mixed_group_id"], name: "index_module_deadlines_on_mixed_group_id"
  add_index "module_deadlines", ["module_max_score_1"], name: "index_module_deadlines_on_module_max_score_1"
  add_index "module_deadlines", ["module_max_score_2"], name: "index_module_deadlines_on_module_max_score_2"
  add_index "module_deadlines", ["module_max_score_3"], name: "index_module_deadlines_on_module_max_score_3"
  add_index "module_deadlines", ["module_max_score_4"], name: "index_module_deadlines_on_module_max_score_4"
  add_index "module_deadlines", ["module_max_score_5"], name: "index_module_deadlines_on_module_max_score_5"
  add_index "module_deadlines", ["profession_id"], name: "index_module_deadlines_on_profession_id"
  add_index "module_deadlines", ["study_year_id"], name: "index_module_deadlines_on_study_year_id"
  add_index "module_deadlines", ["subject_id"], name: "index_module_deadlines_on_subject_id"

  create_table "module_sheets", force: :cascade do |t|
    t.integer  "module_deadline_id"
    t.integer  "student_id"
    t.integer  "module_score_1"
    t.integer  "module_score_2"
    t.integer  "module_score_3"
    t.integer  "module_score_4"
    t.integer  "module_score_5"
    t.integer  "retake"
    t.integer  "individual_task"
    t.integer  "checkout"
    t.string   "ECTS_summary"
    t.integer  "national_summary"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "module_sheets", ["student_id"], name: "index_module_sheets_on_student_id"

  create_table "positions", force: :cascade do |t|
    t.string "name"
  end

  create_table "professions", force: :cascade do |t|
    t.string  "name"
    t.string  "code"
    t.string  "group_alias"
    t.integer "department_id"
  end

  add_index "professions", ["department_id"], name: "index_professions_on_department_id"

  create_table "rights", force: :cascade do |t|
    t.string "class_name"
    t.string "method_name"
    t.string "alias"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
  end

  add_index "roles", ["name"], name: "index_roles_on_name", unique: true

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "roles_users", ["role_id"], name: "index_roles_users_on_role_id"
  add_index "roles_users", ["user_id", "role_id"], name: "index_roles_users_on_user_id_and_role_id", unique: true
  add_index "roles_users", ["user_id"], name: "index_roles_users_on_user_id"

  create_table "settings", force: :cascade do |t|
    t.string "name"
    t.string "value"
  end

  create_table "students", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.boolean  "sex"
    t.date     "birth_date"
    t.integer  "group_id"
    t.string   "student_card"
    t.string   "grade_book"
    t.boolean  "graduated",    default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "students", ["grade_book"], name: "index_students_on_grade_book"
  add_index "students", ["graduated"], name: "index_students_on_graduated"
  add_index "students", ["group_id"], name: "index_students_on_group_id"
  add_index "students", ["student_card"], name: "index_students_on_student_card"

  create_table "study_years", force: :cascade do |t|
    t.string "alias"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.string "code"
  end

  add_index "subjects", ["code"], name: "index_subjects_on_code", unique: true

  create_table "users", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "worker_id"
    t.string   "login"
    t.string   "email"
    t.string   "password_digest"
    t.string   "locale"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["login"], name: "index_users_on_login", unique: true
  add_index "users", ["student_id"], name: "index_users_on_student_id"
  add_index "users", ["worker_id"], name: "index_users_on_worker_id"

  create_table "workers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.boolean  "sex"
    t.date     "birth_date"
    t.integer  "position_id"
    t.boolean  "is_disabled"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
