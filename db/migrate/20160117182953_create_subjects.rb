class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :name
      t.string :code
    end
    add_index :subjects, :code, unique: true
  end
end
