class CreateRights < ActiveRecord::Migration
  def change
    create_table :rights do |t|
      t.string :class_name
      t.string :method_name
      t.string :alias
    end
  end
end
