class CreateMixedGroupsStudentsJoinTable < ActiveRecord::Migration
  def change
    create_table :mixed_groups_students, :id => false do |t|
      t.belongs_to :student, index: true
      t.belongs_to :mixed_group, index: true
    end
    add_index :mixed_groups_students, [:student_id, :mixed_group_id], unique: true
  end
end
