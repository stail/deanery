class CreateMixedGroups < ActiveRecord::Migration
  def change
    create_table :mixed_groups do |t|
      t.belongs_to :department, index: true
      t.string :alias
      t.string :description
      t.date :year
      t.boolean :closed
    end
  end
end
