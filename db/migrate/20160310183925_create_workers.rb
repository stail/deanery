class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.boolean :sex
      t.date :birth_date
      t.belongs_to :position
      t.boolean :is_disabled

      t.timestamps null: false
    end
  end
end
