class CreateModuleDeadlines < ActiveRecord::Migration
  def change
    create_table :module_deadlines do |t|
      t.belongs_to :department, index: true
      t.belongs_to :profession, index: true
      t.belongs_to :group, index: true, :default => :null
      t.belongs_to :mixed_group, index: true, :default => :null
      t.belongs_to :subject, index: true

      t.integer :lecturer_id
      t.integer :assistant_id
      t.integer :dean_id
      t.boolean :is_signed, index: true, :default => false
      t.belongs_to :study_year, index: true
      t.integer :semester
      t.integer :count_of_modules

      t.integer :module_max_score_1, index: true, :default => :null
      t.integer :module_max_score_2, index: true, :default => :null
      t.integer :module_max_score_3, index: true, :default => :null
      t.integer :module_max_score_4, index: true, :default => :null
      t.integer :module_max_score_5, index: true, :default => :null
      t.integer :individual_task_max_score, index: true, :default => :null
      t.integer :exam_max_score, index: true, :default => :null

      t.date :deadline_module_1, index: true, :default => :null
      t.date :deadline_module_2, index: true, :default => :null
      t.date :deadline_module_3, index: true, :default => :null
      t.date :deadline_module_4, index: true, :default => :null
      t.date :deadline_module_5, index: true, :default => :null
      t.date :deadline_individual_task, index: true, :default => :null

      t.date :deadline_checkout

      t.timestamps null: false
    end
  end
end
