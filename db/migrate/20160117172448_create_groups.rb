class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.integer :year
      t.belongs_to :profession, index: true
      t.string :alias
      t.boolean :closed
    end
  end
end
