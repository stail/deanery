class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.belongs_to :student, index: true, :default => :null
      t.belongs_to :worker, index: true, :default => :null
      t.string :login
      t.string :email
      t.string :password_digest
      t.string :locale

      t.timestamps null: false
    end
    add_index :users, :login, :unique => true
    add_index :users, :email, :unique => true
  end
end
