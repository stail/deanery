class CreateProfessions < ActiveRecord::Migration
  def change
    create_table :professions do |t|
      t.string :name
      t.string :code
      t.string :group_alias
      t.belongs_to :department, index: true
    end
  end
end
