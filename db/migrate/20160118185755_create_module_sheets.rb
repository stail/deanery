class CreateModuleSheets < ActiveRecord::Migration
  def change
    create_table :module_sheets do |t|
      t.belongs_to :module_deadline

      t.belongs_to :student, index: true

      t.integer :module_score_1
      t.integer :module_score_2
      t.integer :module_score_3
      t.integer :module_score_4
      t.integer :module_score_5
      t.integer :retake
      t.integer :individual_task
      t.integer :checkout
      t.string :ECTS_summary
      t.integer :national_summary

      t.timestamps null: false
    end
  end
end
