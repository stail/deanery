class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.boolean :sex
      t.date :birth_date

      t.belongs_to :group, index: true
      t.string :student_card, index: true
      t.string :grade_book, index: true
      t.boolean :graduated, index: true, :default => false

      t.timestamps null: false
    end
  end
end
