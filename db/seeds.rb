# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(login: 'Stas', email: 'stas@c.c', password: 'qwerty', password_confirmation: 'qwerty', locale: 'uk', student_id: 1)
User.create(login: 'Jon', email: 'a@b.c', password: 'qwerty', password_confirmation: 'qwerty', locale: 'uk', worker_id: 2)
User.create(login: 'Yura', email: 'b@b.c', password: 'qwerty', password_confirmation: 'qwerty', locale: 'uk', student_id: 3)



Role.create(name: 'root')
Role.create(name: 'admin')
Role.create(name: 'teacher')
Role.create(name: 'student')

RolesUser.create(user_id: 1, role_id: 1)
RolesUser.create(user_id: 1, role_id: 2)
RolesUser.create(user_id: 2, role_id: 3)

Subject.create(name: 'Економічна кібернетика', code: '0001')
Subject.create(name: 'Дослідження операцій', code: '0002')
Subject.create(name: 'Економетрика', code: '0003')
Subject.create(name: 'Фінанси', code: '0004')
Subject.create(name: 'Бухгалтерський облік', code: '0005')
Subject.create(name: 'Інформатика', code: '0006')

Department.create(name: 'Економічний факультет', dean: 3)
Department.create(name: 'Філософський')

StudyYear.create(alias: '2015/2016')

Chair.create(name: 'Кафедра економіко-математичного моделювання', department_id: 1)
Chair.create(name: 'Кафедра обліку, аналізу і аудиту', department_id: 1)
Chair.create(name: 'Кафедра фінансів і кредиту', department_id: 1)

Profession.create(name: 'Економічна кібернетика', code: '6.030502', group_alias: '21', department_id: 1)
Profession.create(name: 'Облік і аудит', code: '6.030509', group_alias: '91', department_id: 1)
Profession.create(name: 'Фінанси і кредит', code: '6.030508', group_alias: '81', department_id: 1)

Profession.create(name: 'Релігія', code: '6.100.500', group_alias: '52', department_id: 2)

Group.create(year: 2013, profession_id: 1, alias: '21')
Group.create(year: 2014, profession_id: 1, alias: '21')
Group.create(year: 2014, profession_id: 2, alias: '91')
Group.create(year: 2013, profession_id: 3, alias: '81')
Group.create(year: 2014, profession_id: 3, alias: '81')


Student.create(first_name: 'Ільницький', last_name: 'Станіслав', middle_name: 'Вікторович', sex: false, birth_date: '1996-04-06', group_id: 1, student_card: '123', grade_book: '1234')
Student.create(first_name: 'Петренко', last_name: 'Василь', middle_name: 'Іванович', sex: false, birth_date: '1996-09-16', group_id: 1, student_card: '124', grade_book: '1244')
Student.create(first_name: 'Бойчук', last_name: 'Юрій', middle_name: 'Романович', sex: false, birth_date: '1997-06-13', group_id: 1, student_card: '1283', grade_book: '12394')
Student.create(first_name: 'Іванов', last_name: 'Олег', middle_name: 'Петрович', sex: false, birth_date: '1996-12-10', group_id: 1, student_card: '125', grade_book: '1254')
Student.create(first_name: 'Зайцев', last_name: 'Микола', middle_name: 'Вадимович', sex: false, birth_date: '1996-11-11', group_id: 2, student_card: '126', grade_book: '1264')
Student.create(first_name: 'Чайка', last_name: 'Олена', middle_name: 'Миколаївна', sex: true, birth_date: '1996-05-08', group_id: 2, student_card: '127', grade_book: '1274')
Student.create(first_name: 'Іванова', last_name: 'Анастасія', middle_name: 'Михайлівна', sex: true, birth_date: '1996-04-21', group_id: 3, student_card: '128', grade_book: '1284')
Student.create(first_name: 'Шевченко', last_name: 'Альона', middle_name: 'Олександрівна', sex: true, birth_date: '1996-07-30', group_id: 3, student_card: '129', grade_book: '1294')
Student.create(first_name: 'Олійник', last_name: 'Ольга', middle_name: 'Ігорівна', sex: true, birth_date: '1996-02-28', group_id: 4, student_card: '120', grade_book: '1214')
Student.create(first_name: 'Вишневська', last_name: 'Марина', middle_name: 'Сергіївна', sex: true, birth_date: '1996-03-26', group_id: 5, student_card: '133', grade_book: '1224')

Position.create(name: 'Викладач')
Position.create(name: 'Асистент')
Position.create(name: 'Професор')
Position.create(name: 'Декан')

Worker.create(position_id: 1, first_name: 'Іванов', last_name: 'Іван', middle_name: 'Іванович', sex: false, birth_date: '1996-09-16')
Worker.create(position_id: 2, first_name: 'Петров', last_name: 'Василь', middle_name: 'Васильович', sex: false, birth_date: '1996-09-16')
Worker.create(position_id: 4, first_name: 'Головний', last_name: 'Петро', middle_name: 'Петрович', sex: false, birth_date: '1996-09-16')

ModuleDeadline.create(
    department_id: 1,
    profession_id: 1,
    group_id: 1,
    subject_id: 1,
    lecturer_id: 1,
    assistant_id: 2,
    dean_id: 3,
    is_signed: false,
    study_year_id: 1,
    semester: 2,
    count_of_modules: 2,
    module_max_score_1: 30,
    module_max_score_2: 30,
    exam_max_score: 30,
    deadline_module_1: '15-03-2016',
    deadline_module_2: '15-05-2016',
    deadline_checkout: '15-06-2016',
)

ModuleSheet.create(
    module_deadline_id: 1,
    student_id: 1,
    module_score_1: 10,
    module_score_2: 10,
)
ModuleSheet.create(
    module_deadline_id: 1,
    student_id: 2,
    module_score_1: 10,
)
ModuleSheet.create(
    module_deadline_id: 1,
    student_id: 3,
    module_score_1: 10,
)
ModuleSheet.create(
    module_deadline_id: 1,
    student_id: 4,
    module_score_1: 10,
)

MixedGroup.create(alias: '300Б',description: 'Бізнес презентації',year: 2016,department_id:1)
MixedGroup.create(alias: '301A',description: 'Психологія',year: 2016,department_id:1)

MixedGroupsStudent.create(student_id:1,mixed_group_id:1)
MixedGroupsStudent.create(student_id:2,mixed_group_id:1)
MixedGroupsStudent.create(student_id:3,mixed_group_id:1)

Setting.create(name: 'current_study_year', value: 1)
Setting.create(name: 'user_root_role', value: 1)
Setting.create(name: 'user_admin_role', value: 2)
Setting.create(name: 'user_teacher_role', value: 3)
Setting.create(name: 'user_student_role', value: 4)