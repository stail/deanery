class TeacherController < DashboardController
  before_action :set_deadline, only: [:show_module, :edit_module, :update_module]

  def index
  end

  def modules
    uid = current_user.id
    @deadlines = ModuleDeadline.where("lecturer_id = #{uid} OR assistant_id = #{uid}").page(params[:page])
  end

  def show_module
  end

  def edit_module
  end

  def update_module
    v = []
    @deadline.students.each do |s|
      m = s.get_module_sheet(@deadline.id)
      m.update(deadline_module_sheet_params(m.id))
      v.push(m)
    end
    render json: {
        success: true,
        message: __('Saved'),
    }
  end

  private
  def set_deadline
    @deadline = ModuleDeadline.find(params[:id])
  end

  def deadline_module_sheet_params(module_sheet_id)
    param_source = params.require(:m)
    logger.debug "module: #{param_source[module_sheet_id.to_s]}"
    param_source[module_sheet_id.to_s].permit(
        :module_score_1,
        :module_score_2,
        :module_score_3,
        :module_score_4,
        :module_score_5,
    )
  end
end
