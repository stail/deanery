class DashboardController < ApplicationController
  before_action :require_user
  helper_method :dashboard_name
  #before_action :require_rights, except: [:index]

  add_breadcrumb :dashboard_name, :dashboard_path

  def index
  end

=begin
  @Right: Access to admin part;
=end
  def admin
    # render :text => __method__
    render :text => current_user.get_roles
  end

=begin
  @Right: Access to other part;
=end
  def test
    render :json => current_user
  end

  protected

  def dashboard_name
    __('Dashboard')
  end
end
