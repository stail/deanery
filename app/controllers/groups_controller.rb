class GroupsController < DashboardController

  helper_method :b_group_name, :b_current_group

  add_breadcrumb :b_group_name, :management_groups_path
  def index
    @groups = Group.page(params[:page])
  end

  def show
    add_breadcrumb :b_current_group
    #Todo: check type of param == integer
    @group = Group.find(params[:id])
  end

  def new
    add_breadcrumb __('New group')
    @group = Group.new
  end

  def create
    @group = Group.new(group_params)
    @group.closed = false
    @group.save
    redirect_to '/storage/group/' + @group.id.to_s
  end

  protected

  def b_group_name
    __('Group Management')
  end

  def b_current_group
    __('Group') + ' ' + @group.get_code
  end

  private

  def group_params
    params.require(:group).permit(
      :year,
      :profession_id,
      :alias
    )
  end
end
