class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_locale
  helper_method :current_user, :root_name, :require_root, :require_teacher
  add_breadcrumb :root_name, :root_path

  def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def require_root
    role_id = Setting.find_by(name: 'user_root_role').value
    role_name = Role.find(role_id).name
    unless current_user.is(role_name)
      redirect_to dashboard_path
    end
  end

  def require_admin
    role_id = Setting.find_by(name: 'user_admin_role').value
    role_name = Role.find(role_id).name
    unless current_user.is(role_name)
      redirect_to dashboard_path
    end
  end

  def require_teacher
    role_id = Setting.find_by(name: 'user_teacher_role').value
    role_name = Role.find(role_id).name
    current_user.is(role_name)
    unless current_user.is(role_name)
      redirect_to dashboard_path
    end
  end

  def require_student
    role_id = Setting.find_by(name: 'user_student_role').value
    role_name = Role.find(role_id).name
    unless current_user.is(role_name)
      redirect_to dashboard_path
    end
  end

  def require_user
    redirect_to login_url unless current_user
  end

  # translation function
  # def __(text)
  #   I18n.t(text).include?('translation missing') ? text : I18n.t(text)
  # end

  protected

  def require_rights
    #render :text => self.class
    unless current_user.get_roles.include?(self.class.to_s+'::'+params[:action])
      raise 'not authorized right'
    end
  end

  def root_name
    __('Home')
  end

  private

  def set_locale
    current_user != nil ?  I18n.locale = current_user.locale : I18n.default_locale
  end

end
