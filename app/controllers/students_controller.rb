class StudentsController < DashboardController
  helper_method :b_student_management_name
  add_breadcrumb :b_student_management_name, :management_students_path
  def index
    @students = Student.page(params[:page])
  end

  def new
    add_breadcrumb __('New Student'), :management_students_path
    @student = Student.new
  end

  def b_student_management_name
    __('Student Management')
  end
end
