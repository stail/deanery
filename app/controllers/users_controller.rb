class UsersController < ApplicationController
  before_action :require_user, only: [:change_locale]
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_url
    else
      redirect_to '/dashboard'
    end
  end

  def change_locale
    @user = current_user
    @user.locale = params[:lang].to_s
    @user.save
    render json: {
        success: true,
        message: __('Language has been changed'),
        data: {
          lang: @user.locale.to_s
        }
    }
  end

  private

  def user_params
    params.require(:user).permit(:login, :email, :password, :password_confirmation)
  end
end
