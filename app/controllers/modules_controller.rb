class ModulesController < DashboardController
  before_action :require_root
  helper_method :b_modules_name, :b_current_module, :b_new_module

  add_breadcrumb :b_modules_name, :management_modules_path

  def index
    @deadlines = ModuleDeadline.page(params[:page])
  end


  def show
    add_breadcrumb :b_current_module
    #Todo: check type of param == integer
    @deadline = ModuleDeadline.find(params[:id])
  end

  def new
    add_breadcrumb :b_new_module
    @module = ModuleDeadline.new
  end

  def create
    @module = ModuleDeadline.new(module_params)
    @module.dean_id = Department.find(module_params[:department_id]).dean
    @module.study_year_id = current_study_year.id

    if params[:use_mixed_group]
      @module.group = nil
    else
      @module.mixed_group = nil
    end

    #set modules score and deadline
    for i in 1..module_params[:count_of_modules].to_i do
      deadline_method_string = 'deadline_module_'+i.to_s
      max_score_method_string = 'module_max_score_'+i.to_s
      date = Date.parse(params[:module_deadline][deadline_method_string])
      score = params[:module_deadline][max_score_method_string]

      @module.send(deadline_method_string+'=', date)
      @module.send(max_score_method_string+'=', score)
    end
    @module.save

    @module.students.each do |m|
      sheet = ModuleSheet.new
      sheet.module_deadline_id = @module.id
      sheet.student_id = m.id
      sheet.save
    end

    redirect_to '/storage/module/' + @module.id.to_s
  end

  protected

  def b_modules_name
    __('Modules Management')
  end

  def b_current_module
    __('Module') + ' ' + @deadline.id.to_s
  end

  def b_new_module
    __('New Module')
  end

  private

  def module_params
    params.require(:module_deadline).permit(
        :department_id,
        :profession_id,
        :group_id,
        :mixed_group_id,
        :subject_id,
        :lecturer_id,
        :assistant_id,
        :count_of_modules,
        :semester,
        :individual_task_max_score,
        :exam_max_score,
        :deadline_individual_task,
        :deadline_checkout
    )
  end
end
