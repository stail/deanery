class ApiController < ApplicationController
  def index
    render json: val = {
        api: 0.1
    }
  end

  def departments
    render json: Department.all.as_json
  end

  def professions
    render json: Profession.where(department_id: params[:department])
  end

  def groups
    render json: Group.where(profession_id: params[:profession])
  end

  def students
    if params[:mixed_group] != nil
      render json: MixedGroupsStudent
                       .select('*')
                       .joins(:student)
                       .where(mixed_group_id: params[:mixed_group])
                       .as_json(only: [:first_name, :last_name, :middle_name])
    else
      render json: Student
                       .where(group_id: params[:group])
                       .as_json(only: [:first_name, :last_name, :middle_name])
    end
  end
end
