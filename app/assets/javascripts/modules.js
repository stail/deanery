function initAddModule() {
    function addRowNumbersColumn(selector) {
        var n = $(selector);
        n.find('tbody').prepend('<tr class="text-center"></tr>');
        var target = n.find('tbody tr:eq(0)');
        n.find('th').each(function (i, e) {
            var tpl = '<td>' + (i + 1) + '</td>';
            target.append(tpl);
        });
    }

    addRowNumbersColumn('.module-sheet-preview');

    var s = {
        department: '#module_deadline_department_id',
        profession: '#module_deadline_profession_id',
        group: '#module_deadline_group_id',
        mixedGroup: '#module_deadline_mixed_group_id',
        useMixedGroup: '#use_mixed_group',
        countModules: '#module_deadline_count_of_modules',
        //todo: try make code pretty
        groupContainer: $('#module_deadline_group_id').parents('.form-group'),
        mixedGroupContainer: $('#module_deadline_mixed_group_id').parents('.form-group'),

        studentsTable: '#modulesStudentsPreviewTable',
        studentsRows: '#modulesStudentsPreviewRows',
        moduleMaxScoreContainer: '.module-score-container',
        moduleDeadlineContainer: '.module-deadline-container',
        deadlineCheckout: '#module_deadline_deadline_checkout',
        individualTaskDeadline: '#module_deadline_deadline_individual_task'
    }

    function drawOptions(selector, array) {
        $(selector).empty();
        $(array).each(function (i, e) {
            var tpl = document.createElement('option');
            $(tpl).val(e.id);
            $(tpl).text(e.name);

            $(selector).append(tpl);
        });
    }

    function groupCode(g) {
        var course = DATE.year - g.year
        if (course == 0) {
            course = 1;
        } else if (DATE.month > 8) {
            course = course + 1
        }
        return course + g.alias;
    }

    function renderProfession() {
        var department = $(s.department).val();
        $.ajax({
            type: 'GET',
            url: '/api/professions/',
            data: {
                'department': department
            },
            success: function (r) {
                drawOptions(s.profession, r);
                renderGroup();
            }
        });
    }

    function renderGroup() {
        var profession = $(s.profession).val();
        $.ajax({
            type: 'GET',
            url: '/api/groups/',
            data: {
                'profession': profession
            },
            success: function (r) {
                $(r).each(function (i, e) {
                    e.name = groupCode(e);
                });
                drawOptions(s.group, r);
                renderStudents(s.mixedGroup + ',' + s.group);
            }
        });
    }

    function drawStudents(r) {
        $(s.studentsRows).empty();
        $(r).each(function (i, e) {
            var fullName = e.first_name + ' ' + e.last_name + ' ' + e.middle_name;
            var tpl = '<tr><td>' + (i + 1) + '</td><td>' + fullName + '</td></tr>';
            $(s.studentsRows).append(tpl);
        });
    }

    function renderStudents(groupSelect) {
        var data = {};
        var value;
        if ($(s.useMixedGroup).is(':checked')) {
            value = $(s.mixedGroup).val();
            data = {
                mixed_group: value
            }
        } else {
            value = $(s.group).val();
            data = {
                group: value
            }
        }
        $.ajax({
            type: 'GET',
            url: '/api/students/',
            data: data,
            success: function (r) {
                $(s.studentsTable).show();
                drawStudents(r);
            }
        });
    }

    //todo: validate count var
    function setModuleMaxScores(count){
        $(s.moduleMaxScoreContainer).each(function(i,e){
            if (i <= count - 1 && i > -1){
                $(e).find('input').prop({
                    'disabled': false,
                    'required': true
                });
                $(this).fadeIn();
            } else {
                $(e).find('input').prop({
                    'disabled': true,
                    'required': false
                });
                $(this).fadeOut();
            }
        });
    }

    function setModuleDeadlines(count){
        $(s.moduleDeadlineContainer).each(function(i,e){
            if (i <= count - 1 && i > -1){
                $(e).find('input').prop({
                    'disabled': false,
                    'required': true
                });
                $(e).find('input').datepicker({
                    dateFormat: "dd/mm/yy"
                });
                $(this).fadeIn();
            } else {
                $(e).find('input').prop({
                    'disabled': true,
                    'required': false
                });
                $(this).fadeOut();
            }
        });
    }
    


    //dynamic change professions from department
    //on load
    renderProfession();
    $('body').on('change', s.department, function () {
        renderProfession();
    });
    //render groups
    $('body').on('change', s.profession, function () {
        renderGroup();
    });
    //use mixed group
    //default false
    $(s.useMixedGroup).prop('checked', false);
    $(s.mixedGroupContainer).hide();
    $('body').on('change', s.useMixedGroup, function () {

        $(s.mixedGroupContainer).slideToggle();
        $(s.groupContainer).slideToggle();
        $(s.group).prop({
           "disabled":  $(s.useMixedGroup).is(":checked")
        });
        renderStudents(s.mixedGroup + ',' + s.group);

    });
    //draw students table
    //default active group
    $('body').on('change', s.mixedGroup + ',' + s.group, function () {
        renderStudents(this);
    });
    //select count of modules
    //hide/show module max score and deadline date
    //default 1
    setModuleMaxScores($(s.countModules).val());
    setModuleDeadlines($(s.countModules).val());
    $('body').on('change', s.countModules, function () {
        setModuleMaxScores($(this).val());
        setModuleDeadlines($(this).val());
    });

    //$(s.deadlineCheckout).datepicker({
    //    dateFormat: "dd/mm/yy"
    //});
    //$(s.individualTaskDeadline).datepicker({
    //    dateFormat: "dd/mm/yy"
    //});
}
//direct load page
$('document').ready(function(){
   initAddModule();
});
//for turbo links load page
$(document).on('page:load', initAddModule);