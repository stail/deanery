// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
function initTeacherPage() {
    var s = {
        teacherUpdateModuleForm:'#teacherUpdateModuleForm'
    }

    $('body').on('submit', s.teacherUpdateModuleForm, function(e){
        e.preventDefault();
        sendForm($(this)).done(function (r) {
            alert(r.message);
        });
    });
}

$('document').ready(function () {
    initTeacherPage();
});
//for turbo links load page
$(document).on('page:load', initTeacherPage);