function initCommonVisualHelpers(){
    var s = {
        inputDate: 'input[type="date"]'
    }
    $(s.inputDate).datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    }).attr('type','text');
    $('[data-year-only="true"]').on('change',function(){
        var newVal = $(this).val().replace(/(..\/..\/)/,'');
        $(this).val(newVal);
    });
}
$('document').ready(function () {
    initCommonVisualHelpers();
});
//for turbo links load page
$(document).on('page:load', initCommonVisualHelpers);