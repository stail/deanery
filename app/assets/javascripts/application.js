// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require turbolinks
//= require_tree .
//= require bootstrap

function sendForm(form){
    var n = $(form);
    return $.ajax({
        type: n.attr('method'),
        url: BASE_URL + n.attr('action'),
        data: n.serialize()
    });
}

function changeLang(lang){
    $.ajax({
        type: 'post',
        url: BASE_URL + '/change/lang',
        data: {
            authenticity_token: $('meta[name="csrf-token"]').attr('content'),
            lang: lang
        }
    }).done(function (r) {
        if(r.success) {
            window.location.reload();
        }
    });
}