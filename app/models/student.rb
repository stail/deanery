class Student < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  has_and_belongs_to_many :mixed_groups

  def get_full_name
    self.first_name + ' ' + self.last_name + ' ' + self.middle_name
  end

  def get_group_code
    self.group.get_code
  end

  def get_course
    now = Time.new
    course = now.year - self.group.year
    if (course == 0)
      course = 1;
    elsif (now.month > 8)
      course = course + 1
    end
    course.to_s
  end

  def get_module_sheet(deadline)
    ModuleSheet.find_by(module_deadline_id: deadline, student_id: self.id)
  end

end
