class Worker < ActiveRecord::Base
  belongs_to :user
  def get_full_name
    self.first_name + ' ' + self.last_name + ' ' + self.middle_name
  end
end
