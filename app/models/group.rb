class Group < ActiveRecord::Base
  has_many :students
  belongs_to :profession

  def get_code
    self.get_course + self.alias
  end

  def get_course
    now = Time.new
    course = now.year - self.year
    if (course == 0)
      course = 1;
    elsif (now.month > 8)
      course = course + 1
    end
    course.to_s
  end

  def get_code_profession_name
    self.get_code + ' ' + self.profession.name
  end
end
