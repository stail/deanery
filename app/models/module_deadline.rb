class ModuleDeadline < ActiveRecord::Base
  belongs_to :module_sheet
  belongs_to :department, class_name: 'Department'
  belongs_to :profession
  belongs_to :group
  belongs_to :mixed_group
  belongs_to :subject
  belongs_to :study_year
  has_many :students, through: :mixed_group
  has_many :students, through: :group

  def get_group_code
    self.group != nil ? self.group.get_code : self.mixed_group.alias
  end

  def students
    self.group != nil ? self.group.students : self.mixed_group.students
  end

  def lecturer
    Worker.find(self.lecturer_id)
  end

  def assistant
    Worker.find(self.assistant_id)
  end

  def module_sheets
    ModuleSheet.where(:module_deadline_id => self.id)
  end
end
