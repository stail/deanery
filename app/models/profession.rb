class Profession < ActiveRecord::Base
  belongs_to :department
  has_many :groups
end
