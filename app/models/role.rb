class Role < ActiveRecord::Base
  has_and_belongs_to_many :users
  def set_user(user)
    if !RolesUser.find_by(user_id: user, role_id: self.id) && User.find_by(id: user) != nil
      RolesUser.create(user_id: user, role_id: self.id)
    end
  end
end
