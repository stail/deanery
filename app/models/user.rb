class User < ActiveRecord::Base
  has_and_belongs_to_many :roles
  has_one :student
  has_one :worker
  has_secure_password

  def set_role(role)
    if !RolesUser.find_by(user_id: self.id, role_id: role) && Role.find_by(id: role) != nil
      RolesUser.create(user_id: self.id, role_id: role)
    end
  end

  def is(role_name)
    if Role.find_by(name: role_name) != nil
      role = Role.find_by(name: role_name)
    else
      return false
    end
    return RolesUser.find_by(user_id: self.id, role_id: role.id) ? true : false
  end

  def get_roles
    array = []
    User.find(self.id).roles.each { |k| array << 'DashboardController::' + k.name }
    array
  end

  def get_full_name
    #todo:fix reference self.worker, self.student
    if Worker.where(id: self.worker_id).length != 0
      human = Worker.find(self.worker_id)
    else
      human = Student.find(self.student_id)
    end
    human.get_full_name
  end
end
