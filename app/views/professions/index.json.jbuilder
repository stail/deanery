json.array!(@professions) do |profession|
  json.extract! profession, :id, :name, :code, :group_alias, :department_id
  json.url profession_url(profession, format: :json)
end
