json.array!(@workers) do |worker|
  json.extract! worker, :id, :first_name, :last_name, :middle_name, :sex, :birth_date, :position_id, :is_disabled
  json.url worker_url(worker, format: :json)
end
