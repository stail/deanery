module RoleHelper
  def role_css_class(role_name)
    case role_name
      when 'root'
         'label-danger'
      when 'admin'
         'label-info'
      when 'student'
         'label-success'
      else
         'label-default'
    end
  end
end
