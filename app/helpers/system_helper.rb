module SystemHelper
  def controller_method_parser

    # desc "Loading all controllers"
    files = []
    #load all the controllers
    controllers = Dir.new("#{Rails.root}/app/controllers").entries
    controllers.each do |entry|
      if entry =~ /_controller/
        #check if the controller is valid
        files << File.read("#{Rails.root}/app/controllers/#{entry}").to_s
      elsif entry =~ /^[a-z]*$/ #namescoped controllers
        Dir.new("#{Rails.root}/app/controllers/#{entry}").entries.each do |x|
          if x =~ /_controller/
            files << File.read("#{Rails.root}/app/controllers/#{entry}").to_s
          end
        end
      end
    end

    right_collection = []
    files.each do |f|

      controller_name = f.scan(/\s*class\s*(\S*)\s*<\s*ApplicationController/)
      method_names = f.scan(/\=begin\s*@Right:\s*.*;\s*=end\s*def\s*(\S*)\s*/)
      right_names = f.scan(/\=begin\s*@Right:\s*(.*);\s*=end\s*def/)

      if controller_name != nil && method_names.length > 0
        right_collection << {
            controller: controller_name[0],
            methods: method_names,
            rights: right_names,
        }
      end
    end

    right_collection

  end
end
