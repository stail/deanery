module ApplicationHelper
  def page_title(text)
      content_for :page_title, text.to_s
  end
  def __(text)
    I18n.t(text).include?('translation missing') ? text : I18n.t(text)
  end

  def current_study_year
    StudyYear.find(Setting.find_by(name: 'current_study_year'))
  end

  def sex_list
    [{title: __('Man'), value: false}, {title: __('Woman'), value: true}]
  end
  def base_url
    request.protocol + request.host_with_port
  end
  #Return current language of session if exist or default in string
  def current_lang
    !current_user ? I18n.default_locale.to_s : current_user.locale.to_s
  end
end

