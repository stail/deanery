require 'test_helper'

class WorkersControllerTest < ActionController::TestCase
  setup do
    @worker = workers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:workers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create worker" do
    assert_difference('Worker.count') do
      post :create, worker: { birth_date: @worker.birth_date, first_name: @worker.first_name, is_disabled: @worker.is_disabled, last_name: @worker.last_name, middle_name: @worker.middle_name, position_id: @worker.position_id, sex: @worker.sex }
    end

    assert_redirected_to worker_path(assigns(:worker))
  end

  test "should show worker" do
    get :show, id: @worker
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @worker
    assert_response :success
  end

  test "should update worker" do
    patch :update, id: @worker, worker: { birth_date: @worker.birth_date, first_name: @worker.first_name, is_disabled: @worker.is_disabled, last_name: @worker.last_name, middle_name: @worker.middle_name, position_id: @worker.position_id, sex: @worker.sex }
    assert_redirected_to worker_path(assigns(:worker))
  end

  test "should destroy worker" do
    assert_difference('Worker.count', -1) do
      delete :destroy, id: @worker
    end

    assert_redirected_to workers_path
  end
end
