require 'test_helper'

class TeacherControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get modules" do
    get :modules
    assert_response :success
  end

end
